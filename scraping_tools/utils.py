__author__ = 'jesusrp'

from scraping_tools import scrape_me

def scrape_ingredients(recipe_url):
    """
        Given a recipe website url, scrapes it for ingredient descriptions (phrases) and generates a text file containing them

        Parameters
        ----------
        recipe_url : url of the recipe website

        Return
        ----------
        a text file with the name of the recipe containing the ingredient descriptions
    """

    scrape = scrape_me(recipe_url)

    for i in range(len(recipe_url)):
        if recipe_url[i] == '?':
            break

    if i < len(recipe_url)-1:
        recipe_url = recipe_url[:i-1]

    if recipe_url[-1] == '/':
        recipe_name = recipe_url[:-1].rsplit('/', 1)[-1]
    else:
        recipe_name = recipe_url.rsplit('/', 1)[-1]

    with open('./data/'+recipe_name+'.txt', 'w') as ingredients_crf:
        for ing in scrape.ingredients():
            ingredients_crf.write(ing+'\n')

    return recipe_name
