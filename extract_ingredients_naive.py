__author__ = 'jesusrp'

import csv
import sys
from scraping_tools import utils
from tagging_tools import utils_naive
from matching_tools import _utils


def main():
    """
        Given the name of the text file containing from the /data folder with the ingredient descriptions (phrases)
        extract their quantities, units and names into a csv file

        Parameters
        ----------
        sys.argv[1] : path to the text file with the ingredient descriptions

        Return
        ----------
        a csv file with the name of the input file containing the ingredients qty, unit and name fields
    """

    recipe_url = sys.argv[1]
    recipe_name = utils.scrape_ingredients(recipe_url)

    web_ingredient_descs = open('./data/'+recipe_name+'.txt', "r").readlines()
    qty_unit_names = [[utils_naive.Ingredient(web_ingredient_desc).qty, utils_naive.Ingredient(web_ingredient_desc).unit, ",".join(_utils.prettify_web_ingredient_name(utils_naive.Ingredient(web_ingredient_desc).name))] for web_ingredient_desc in web_ingredient_descs]

    with open('./data/'+recipe_name + "_naive.csv", "w") as myfile:
        wr = csv.writer(myfile)
        wr.writerow(['qty','unit','name'])
        for row in qty_unit_names:
            wr.writerow(row)

    return

if __name__ == "__main__":
    main()
