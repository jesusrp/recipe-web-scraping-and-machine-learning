Instructions (support for Python 3.5)

- Install crf++ (if on Linux or Windows, do so from https://taku910.github.io/crfpp/)
    tested on Linux

- Once the repository is cloned, from the root folder run:

pip install -r requirements.txt

python3.5 extract_ingredients_naive.py [url of the recipe website]
    generates a csv file in the /data folder with the quantity, unit and name of each ingredient
    for such recipe

python3.5 extract_ingredients_crf.py [url of the recipe website]
    generates a csv file in the /data folder with the quantity, unit and name of each ingredient
    for such recipe

Outputs provided in the /data folder for recipe websites:

    'https://www.allrecipes.com/recipe/59124/apple-turnovers/?clickId=right%20rail0&internalSource=rr_feed_recipe_sb&referringId=8170%20referringContentType%3Drecipe'
    'https://www.allrecipes.com/recipe/219965/lighter-chicken-fettuccine-alfredo/?internalSource=staff%20pick&referringId=95&referringContentType=recipe%20hub'


