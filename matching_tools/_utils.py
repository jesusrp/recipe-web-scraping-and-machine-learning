__author__ = 'jesusrp'

from scraping_tools import scrape_me
import openpyxl
import os
from nltk.tokenize import regexp_tokenize
from tagging_tools import utils_naive


def prettify_web_ingredient_name(web_ingredient_name):
    """
    Rewrites ingredient name in USDA-like form (a comma-separated list of the words in reverse order)
    """

    if web_ingredient_name == '':
        pretty_name = []
    else:
        pretty_name = [web_ingredient_name[i] for i in range(len(web_ingredient_name)-1,-1,-1)]

    return pretty_name


def preprocess_db_ingredient_pretty_name(db_ingredient_pretty_name):
    """
        Preprocesses a given a Kitchry database ingredient pretty name.

        Parameters
        ----------
        db_ingredient_name : database ingredient name

        Return
        ----------
        preprocessed database ingredient name
    """

    if db_ingredient_pretty_name == '':
        preprocessed_db_ingredient_name = []
    else:
        preprocessed_db_ingredient_name = [token.lower() for token in regexp_tokenize(db_ingredient_pretty_name, pattern=r"\s|[\.,;']", gaps=True)]

    return preprocessed_db_ingredient_name


def match_web_ingredient_name_against_db(web_ingredient_name, db_ingredient_names):
    """
        Given a web ingredient name and a db ingredient name list, attempts to match the ingredient name against the
        name list.

        Parameters
        ----------
        ingredient_name : web ingredient name
        ingredient_names: list of database ingredient names

        Return
        ----------
        [database ingredient name, web ingredient name] if there is a match, else 'NOMATCH'
    """

    for i in range(len(db_ingredient_names)):
        if preprocess_db_ingredient_pretty_name(db_ingredient_names[i]) != '' and prettify_web_ingredient_name(web_ingredient_name) != '':
            if preprocess_db_ingredient_pretty_name(db_ingredient_names[i]) == prettify_web_ingredient_name(web_ingredient_name):
                return [db_ingredient_names[i], web_ingredient_name]

    return 'NOMATCH'


def match_web_ingredients_against_db(recipe_url):
    """
        Given a recipe website, attempts to match its recipe ingredient names against the Kitchry USDA database

        Parameters
        ----------
        sys.argv[1] : url of the recipe site

        Return
        ----------
        For each ingredient name on the recipe website [database ingredient name, web ingredient name] if there is a match, else 'NOMATCH'
    """

    usda_db_name = '/data/Kitchry USDA database.xlsx'
    db_book = openpyxl.load_workbook(os.getcwd()+usda_db_name)
    db_sheet = db_book.active
    db_ingredient_pretty_names = db_sheet['D']
    db_ingredient_pretty_names = [db_ingredient_pretty_names[i].value for i in range(1,len(db_ingredient_pretty_names))]

    scrape = scrape_me(recipe_url)
    match_result = [match_web_ingredient_name_against_db(utils_naive.Ingredient(web_ingredient_desc).name, db_ingredient_pretty_names) for web_ingredient_desc in scrape.ingredients()]

    web_names = [prettify_web_ingredient_name(utils_naive.Ingredient(web_ingredient_desc).name) for web_ingredient_desc in scrape.ingredients()]
    db_names = [preprocess_db_ingredient_pretty_name(db_ingredient_pretty_name) for db_ingredient_pretty_name in db_ingredient_pretty_names]

    return (match_result, web_names, db_names)


