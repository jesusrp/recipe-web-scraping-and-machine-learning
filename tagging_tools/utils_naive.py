__author__ = 'jesusrp'

from nltk.tokenize import regexp_tokenize

class Ingredient(object):
    """
    An Ingredient object.

    Attributes
    ----------
    qty : quantity (cardinal) of the ingredient amount
    unit: spoon, teaspoon, mg, litres...
    name: food and presentation (sliced, peeled...)
    """

    def __init__(self, ingredient_desc):
        """
        - Initializes ingredient attributes from an ingredient description in text form
            e.g. '2 cups all-purpose flour'
        - Assumes [qty] [unit] [name] format

        Parameters
        ----------
        ingredient_desc : ingredient_description in text form
        """

        tokenized_ingredient = [token.lower() for token in regexp_tokenize(ingredient_desc, pattern=r"\s|[\.,;']", gaps=True) if token not in ['and','or','-']]
        self.qty = tokenized_ingredient[0]
        self.unit = tokenized_ingredient[1]
        self.name = tokenized_ingredient[2:]

