__author__ = 'jesusrp'

import os
import csv
import sys

from scraping_tools import utils
from tagging_tools import utils_crf


def main():
    """
        Given the url of a recipe website extract their ingredient quantities, units
        and names into a csv file

        Parameters
        ----------
        sys.argv[1] : url of the recipe website

        Return
        ----------
        a csv file with the name of the input file containing the ingredients qty, unit and name fields
    """

    recipe_url = sys.argv[1]
    recipe_name = utils.scrape_ingredients(recipe_url)

    with open('./data/'+recipe_name+'.txt') as infile, open('./data/temp.txt', 'w') as outfile:
        outfile.write(utils_crf.export_data(infile.readlines()))

    os.putenv('LD_LIBRARY_PATH','/usr/local/lib')
    os.system("crf_test -v 1 -m ./data/model_file ./data/temp.txt > ./data/results.txt")
    os.system("rm ./data/temp.txt")

    data = utils_crf.import_data(open('./data/results.txt'))
    os.system("rm ./data/results.txt")

    qty_unit_names = [[datum['qty'] if datum.get('qty') else '', datum['unit'] if datum.get('unit') else '', datum['name'] if datum.get('name') else ''] for datum in data]

    with open('./data/'+recipe_name + "_crf.csv", "w") as myfile:
        wr = csv.writer(myfile)
        wr.writerow(['qty','unit','name'])
        for row in qty_unit_names:
            wr.writerow(row)

    return

if __name__ == "__main__":
    main()
